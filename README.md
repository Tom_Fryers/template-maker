# Template Maker

Make many files from a template and CSV.

```
python main.py [-i IGNORE] template data
```

The syntax for the template file consists of two parts.
The file path to produce is surrounded by `@` signs, followed by the file
contents. This unit can be repeated multiple times, which will produce
multiple files per row of the CSV.

To insert the data from the CSV into the file, use `$0` for the first
column, `$1` for the second etc.

The following escape sequences are supported:

1. `\\`: \
2. `\@`: @
3. `\$`: \$
4. `\n`: [new line]
5. `\t`: [tab]
6. `\Uxxxxxxxx`: [Unicode point xxxxxxxx (substitute hexadecimal digits
   for `x`s; all eight are required)]

A backslash before any other character will ignore it. This is useful
particularly with newlines.
