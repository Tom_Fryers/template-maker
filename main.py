#!/usr/bin/env python3
"""Generate many files from a template and CSV."""

import argparse
import csv
import pathlib


class ParseError(Exception):
    pass


def get_unicode_character(instructions: list) -> str:
    """Pop eight hexadecimal characters, returning the unicode character
    they correspond to."""
    if len(instructions) < 8:
        raise ParseError("unicode escape sequence requires eight characters")
    try:
        unicode_value = instructions[:8]
        instructions = instructions[8:]
        return chr(int("".join(unicode_value), 16))
    except ValueError:
        raise ParseError("invalid hexadecimal Unicode character: {unicode_value}")


def parse(instructions):
    """Convert the instructions text into a list of tokens."""
    instructions = list(instructions)
    templates = []
    parsing_file_pattern = False
    while instructions:
        character = instructions.pop(0)
        # Escape sequences
        if character == "\\":
            literal_char = instructions.pop(0)
            if literal_char == "U":
                templates[-1][-1].append(get_unicode_character(instructions))
            elif literal_char == "n":
                templates[-1][-1].append("\n")
            elif literal_char == "t":
                templates[-1][-1].append("\t")
            elif literal_char in r"\@$":
                templates[-1][-1].append(character)
        # Reserved tokens
        elif character == "@":
            if parsing_file_pattern:
                parsing_file_pattern = False
                templates[-1].append([])
                if instructions[0] == "\n":
                    instructions.pop(0)
                continue
            parsing_file_pattern = True
            templates.append([[]])
        elif character == "$":
            index = []
            while (index_char := instructions.pop(0)) in "0123456789":
                index.append(index_char)
            templates[-1][-1].append(int("".join(index)))
            templates[-1][-1].append(index_char)
        # Everything else is literal
        else:
            templates[-1][-1].append(character)
    return templates


def parse_file(file_object):
    """Parse a file to create a template."""
    with file_object as f:
        data = f.read()
    return parse(data)


def collapse_pattern(pattern, row):
    """Substitute the values from a CSV ROW into the pattern."""
    result = []
    for element in pattern:
        if isinstance(element, str):
            result.append(element)
        else:
            result.append(row[element])
    return "".join(result)


def make_files(templates, csv):
    """Create the files using the templates and CSV."""
    for row in csv:
        for pattern in templates:
            filename = collapse_pattern(pattern[0], row)
            pathlib.Path(filename).parents[0].mkdir(parents=True, exist_ok=True)
            with open(filename, "w") as f:
                f.write(collapse_pattern(pattern[1], row))


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "template",
        type=argparse.FileType("r"),
        help="file giving the structure to produce",
    )
    parser.add_argument("data", type=argparse.FileType("r"), help="CSV file to use")
    parser.add_argument(
        "-i",
        "--ignore",
        metavar="NUMBER",
        type=int,
        default=0,
        help="ignore this number of initial rows",
    )
    args = parser.parse_args()
    patterns = parse_file(args.template)

    make_files(patterns, list(csv.reader(args.data))[args.ignore :])


if __name__ == "__main__":
    main()
